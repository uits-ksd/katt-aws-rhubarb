class Rhubarb::NetKernel
  # Namespace for several file utility methods for copying, moving, removing, etc.
  include FileUtils
  require 'net/http'
  require 'uri'

  attr_accessor :logger, :status_timeout, :status_sleep, :parsed_uri

  delegate :debug, :info, :warn, :error, :fatal, :log_to_stdout, to: :@logger

  # Rhubarb::NetKernel must be initialized with a pathname.
  # Examples: kfsjpmccardholder, kfsjpmctransaction
  #
  def initialize(uri)
    @parsed_uri = URI.parse(uri)
  end

  def notify
    Net::HTTP.start(@parsed_uri.host, @parsed_uri.port,
    :use_ssl => @parsed_uri.scheme == 'https') do |http|
      http.read_timeout = 300
      request = Net::HTTP::Get.new @parsed_uri.request_uri
      response = http.request request # Net::HTTPResponse object
      response.body
    end
  end

  def succeeded?(result)
    # result is a Net::HTTPResponse body
    if result.include? "<process><status>ok"
     return true
    end
    
    puts 'Unsuccessful NetKernel trigger'
    puts "Response was: #{result}"
    return false
  end

end
