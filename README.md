# Rhubarb

## Introduction

Rhubarb is a Ruby library which will connect Control-M to KFS via Control-M's 'OS' job type, and KFS's Batch Invoker.

## Compatibility

Rhubarb within docker containers built with:

* **Ruby 3.2.2** on **Amazon Linux 2023** OS image

## Building

Since we are wrapping our Rhubarb deployment within a Dockerfile, there are two considerations when defining the version of this code repository that is included in the Capistrano deployment during the Docker image build.

1. The git branch that is being used when this project is cloned.
2. The "target environment" that corresponds to a deployment stage. For example, in the stg7.rb file, the master branch of the code is hard-coded. If you are doing any sort of "local" testing, the local7.rb file will need to be changed to match the feature branch of this code.

This is further explained in https://bitbucket.org/uits-ksd/docker-rhubarb/src/master/README.md.


## Testing

For local developer testing:

1. Clone this repo
 
2. Ensure path constants in `<PROJECT_DIR>/dev/docker.sh` are correct

3. Open terminal:

    * `$ cd <PROJECT_DIR>/dev`
   
    * `$ ./docker.sh build`
   
    * `$ ./docker.sh run`
   
    * `$ docker exec -it rhubarb bash`
   
    * `# rake`
