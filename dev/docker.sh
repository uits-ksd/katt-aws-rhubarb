#!/bin/bash
################################################################################
# docker.sh - A helper script to run docker commands on a developer workstation.
#
# Usage: $ ./docker.sh <help|build|run|clean>
################################################################################

# These paths are relative to the developer workstation (i.e., outside the container)
PROJECT_DIR="$HOME/git/rhubarb"
HOST_BATCH_DIR="$HOME/env/kfs/transactional"

# These two don't matter so long as they're unique on the host that this container
# is built and run on (i.e., will be installed/pulled from docker daemon's local repo)
DOCKER_REPO_NAME="ua"
IMAGE_NAME="rhubarb"


#
# Print help info when asked to or when `command` is invalid
#
function _usage() {
    echo "$ ./docker.sh <help|build|run|stop|clean>"
    echo "     help: Print these lines"
    echo "    build: (re)Build docker image for when Dockerfile changes"
    echo "      run: Start a new container with needed env vars and mounts"
    echo "     stop: Stops the container"
    echo "    clean: Delete all dangling images, i.e., non-tagged"
}


function _run() {
    docker run \
        -dit \
        -v "$PROJECT_DIR:/usr/src/app" \
        -v "$HOST_BATCH_DIR:/transactional/work" \
        -e "BATCH_HOME=/transactional/work" \
        --rm \
        --name "$IMAGE_NAME" \
        $DOCKER_REPO_NAME/$IMAGE_NAME:latest
}


function _stop() {
    docker stop rhubarb
}


#
# Build/rebuild a new image from the Dockerfile. Note this will 'steal' the tag
# from the current image if it exists. This means the old image will have a name
# that is the same as its id in the `$ docker images` list. Use `_clean` to clear
# out these orphaned images.
#
function _build() {
    docker buildx build\
    --file Dockerfile \
    --tag $DOCKER_REPO_NAME/$IMAGE_NAME:latest \
    .
}


#
# Prune (delete) all images that are 'dangling', i.e., has no associated
# container and has not tagged name. These are the images in the
# '$ docker images` list where name and id are the same. These become
# dangling by way of a new build always tagging the image as
# 'sgs/python-sandbox:latest` -- only the latest build will retain the tag.
#
function _clean() {
    # shellcheck disable=SC2046
    docker rmi $(docker images -f "dangling=true" -q)
}


#
# Driver function, reads user command and executes the related function.
#
function main() {
    if [[ $# -lt 1 ]]; then
        _usage
        exit 1
    fi
    command="$1"

    case "$command" in
    'run')
        _run
        ;;
    'stop')
        _stop
        ;;
    'build')
        _build
        ;;
    'clean')
        _clean
        ;;
    'help')
        _usage
        ;;
    *)
        _usage
        ;;
    esac
}

main "$@"
