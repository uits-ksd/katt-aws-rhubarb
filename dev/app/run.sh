#!/bin/bash

function main() {
    cd /usr/src/app
    bundle install
    tail -f /dev/null
}

main
